/*
* Dependencies
*/
var gulp = require('gulp'),
prefix = require('gulp-autoprefixer'),
opt_img = require('gulp-image-optimization');

gulp.task('prefix', function() {
	gulp.src('src/css/*.css')
	.pipe(prefix({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest('src/css'))
});

gulp.task('opt_img', function(cb) {
	gulp.src(['src/img/*.png']).pipe(opt_img({
		optimizationLevel: 5,
		progressive: true,
		interlaced: true
	})).pipe(gulp.dest('src/img/')).on('end', cb).on('error', cb);
});
